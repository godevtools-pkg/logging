module gitlab.com/godevtools-pkg/logging

go 1.18

require github.com/sirupsen/logrus v1.9.0

require golang.org/x/sys v0.2.0 // indirect
