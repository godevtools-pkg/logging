package logging

import (
	"github.com/sirupsen/logrus"
	"os"
)

type Logg struct {
	*logrus.Entry
}

func GetLogger() *Logg {
	logger := logrus.New()
	logger.SetReportCaller(true)
	logger.Formatter = &logrus.JSONFormatter{
		PrettyPrint: true,
	}

	logger.SetOutput(os.Stdout)
	logger.SetLevel(logrus.DebugLevel)

	return &Logg{logrus.NewEntry(logger)}
}
